import { extendAction } from 'utilities/util';

export const TYPES = {
    LOAD_PHOTO: extendAction('LOAD_PHOTO'),
    SET_TAGS: extendAction('SET_TAGS'),
    LOAD_AUTHOR_PHOTOS: extendAction('LOAD_AUTHOR_PHOTOS')
}

export const getPhotosLoaded = () => ({
    type: TYPES.LOAD_PHOTO.PENDING,
    isLoading: true,
    isError: false
});

export const getPhotosFulfiled = (data: any) => ({
    type: TYPES.LOAD_PHOTO.FULFILLED,
    isLoading: false,
    isError: false,
    data
});

export const getPhotosRejected = (error: any) => ({
    type: TYPES.LOAD_PHOTO.REJECTED,
    isLoading: false,
    isError: true,
    error
});

export const setTagsPending = () => ({
    type: TYPES.SET_TAGS.PENDING,
    isLoading: true,
    isError: false
});

export const setTagsFulfiled = (tags: string) => ({
    type: TYPES.SET_TAGS.FULFILLED,
    isLoading: false,
    isError: false,
    tags
});

export const setTagsRejected = (error: any) => ({
    type: TYPES.SET_TAGS.PENDING,
    isLoading: false,
    isError: true,
    error
});

export const getAuthorPhotosPending = () => ({
    type: TYPES.LOAD_AUTHOR_PHOTOS.PENDING,
    isLoading: true,
    isError: false
});

export const getAuthorPhotosFulfiled = (authorPhotos: any) => ({
    type: TYPES.LOAD_AUTHOR_PHOTOS.FULFILLED,
    isLoading: false,
    isError: false,
    authorPhotos
});

export const getAuthorPhotosRejected = (error: any) => ({
    type: TYPES.LOAD_AUTHOR_PHOTOS.PENDING,
    isLoading: false,
    isError: true,
    error
});