import { getAuthorPhotosFulfiled, getAuthorPhotosRejected } from 'action-types/photos';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { 
    getPhotosLoaded, 
    getPhotosFulfiled, 
    getPhotosRejected,
    setTagsPending,
    setTagsFulfiled,
    setTagsRejected,
    getAuthorPhotosPending
} from 'action-types/photos';
import { callApiGet } from 'utilities/axios';
import { API, IGetPhotosFilters } from 'constants/api';

export const getPhotos = (tags: string = 'dogs', page = 1, perPage = 100, filters: IGetPhotosFilters = {
    sort: '',
    license: '',
    minTakenDate: '',
    maxTakenDate: ''
}, onSuccess?: () => void) => {
    return async (dispatch: ThunkDispatch<any, any, Action>) => {
        dispatch(getPhotosLoaded());
        try {
            const res = await callApiGet(API.GetPhotos(tags, page, perPage, filters));
            dispatch(getPhotosFulfiled(res.data.photos));

            if (onSuccess) {
                onSuccess();
            }
            
        } catch (e) {
            dispatch(getPhotosRejected(e))
        }
    }
}

export const setTags = (tags: string) => {
    return async (dispatch: ThunkDispatch<any, any, Action>) => {
        dispatch(setTagsPending());
        try {
            dispatch(setTagsFulfiled(tags));
        } catch (e) {
            dispatch(setTagsRejected(e))
        }
    }
}

export const getAuthorPhotos = (authorId: string) => {
    return async (dispatch: ThunkDispatch<any, any, Action>) => {
        dispatch(getAuthorPhotosPending());
        try {
            const res = await callApiGet(API.GetAuthorPhotos(authorId));
            dispatch(getAuthorPhotosFulfiled(res.data.photos));
        } catch (e) {
            dispatch(getAuthorPhotosRejected(e));
        }
    }
}