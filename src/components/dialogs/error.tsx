import React from 'react';
import styled from 'styled-components';
import { COLORS } from 'constants/colors';

const SWrapper: any = styled.div`
    width: 80%;
    margin: 0 auto;
    background: ${COLORS.RED};
    color: ${COLORS.WHITE};
    padding: 20px 15px;
    display: ${(props: IErrorDialogProps) => props.visible ? 'flex' :  'none'};
    align-items: center;
    justify-content: space-between;
    box-sizing: border-box;
`;

const SText = styled.p`
    margin: 0;
    font-size: 15px;
`;

const SButton = styled.button`
    background: transparent;
    border: 0;
    color: ${COLORS.WHITE};
    font-weight: 700;
    font-size: 16px;
    cursor: pointer

    &:focus {
        outline: none;
    }
`;

interface IErrorDialogProps {
    visible: boolean;
    onClose: () => void;
}

export const ErrorDialog: React.StatelessComponent<IErrorDialogProps> = (props: IErrorDialogProps) => (
    <SWrapper>
        <SText>Some error has occured. Try again later.</SText>
        <SButton onClick={props.onClose}>close</SButton>
    </SWrapper>
);