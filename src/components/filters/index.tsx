import React, { Component } from 'react';
import styled from 'styled-components';
import { QuarterGrid } from 'components/grid/QuarterGrid';
import { InjectedFormProps, reduxForm, reset } from 'redux-form';
import { Dispatch, compose, Action } from 'redux';
import { FormInput } from 'components/redux-form/input/field';
import { COLORS } from 'constants/colors';
import { IGetPhotosFilters, Sort, License } from 'constants/api';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { getPhotos } from 'actions/photos';
import { IStore } from 'reducers/index';
import { FormSelect } from 'components/redux-form/select/field';
import { convertEnumToOptionsArray, convertEnumToOptionsArrayByValue } from 'utilities/util';

const SWrapper = styled.div`
    width: 100%;
    border: 1px solid #e6e6e6;
    background: ${COLORS.WHITE};
    padding: 20px;
    box-sizing: border-box;
    display: flex;
`;

const SInputWrapper = styled.div`
    display: flex;
    flex-direction: column;
`;

const SLabel = styled.p`
    font-size: 12px;
    margin-top: 0;
    color: ${COLORS.DARK_BLUE};
`;

const SGrid90 = styled.div`
    width: 90%;
`;

const SGrid10 = styled.div`
    width: 10%;
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
`;

const SSendButton = styled.button`
    border: 0;
    background: ${COLORS.RED};
    color: ${COLORS.WHITE};
    height: 54px;
    width: 90%;

    &:focus {
        outline: 0;
    }
`;

interface IMapStateToProps {
    tags: string;
    page: number;
    perPage: number;
}

interface IMapDispatchToProps {
    getPhotos: (tags?: string, page?: number, per_page?: number, filters?: IGetPhotosFilters, onSuccess?: any) => Promise<void>;
}

interface IFiltersProps extends IMapStateToProps, IMapDispatchToProps {
    onSubmit: (tags?: string,page?: number, per_page?: number, filters?: IGetPhotosFilters, onSuccess?: any) => Promise<void>;
}

class _Filters extends Component<InjectedFormProps<{}, IFiltersProps> & IFiltersProps> {
    public render() {
        return(
            <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                <SWrapper>
                    <SGrid90>
                        <QuarterGrid>
                            <SInputWrapper>
                                <SLabel>Sorting:</SLabel>
                                <FormSelect 
                                    name="Sort"
                                    options={convertEnumToOptionsArray(Sort)}
                                />
                            </SInputWrapper>
                            <SInputWrapper>
                                <SLabel>License type:</SLabel>
                                <FormSelect 
                                    name="License"
                                    options={convertEnumToOptionsArrayByValue(License)}
                                />
                            </SInputWrapper>
                            <SInputWrapper>
                                <SLabel>Minimal date taken:</SLabel>
                                <FormInput 
                                    name="MinDate"
                                    placeholder="YYYY-MM-DD"
                                />
                            </SInputWrapper>
                            <SInputWrapper>
                                <SLabel>Maximal date taken:</SLabel>
                                <FormInput 
                                    name="MaxDate"
                                    placeholder="YYYY-MM-DD "
                                />
                            </SInputWrapper>
                        </QuarterGrid>
                    </SGrid90>
                    <SGrid10>
                        <SSendButton type="submit">
                            send
                        </SSendButton>
                    </SGrid10>
                </SWrapper>
            </form>
        );
    }

    private onSubmit = async (fields: any) => {
        const { tags, page, perPage } = this.props;

        this.props.getPhotos(tags, page, perPage, {
            sort: fields.Sort ? fields.Sort : '',
            license: fields.License ? fields.License : '',
            minTakenDate: fields.MinDate ? fields.MinDate : '',
            maxTakenDate: fields.MaxDate ? fields.MaxDate : ''
        });
    }
}

const FORM_NAME = 'FILTERS';

const afterSubmit = (_: any, dispatch: Dispatch) => dispatch(reset(FORM_NAME));

const mapStateToProps = (state: IStore): IMapStateToProps => ({
    tags: state.photos.tags,
    page: state.photos.page,
    perPage: state.photos.perpage
});

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, Action>): IMapDispatchToProps => ({
    getPhotos: (tags?: string, page?: number, per_page?: number, filters?: IGetPhotosFilters, onSuccess?: any) => dispatch(getPhotos(tags, page, per_page, filters, onSuccess))
});

export const Filters = compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm<{}, IFiltersProps>({
        form: FORM_NAME,
        onSubmitSuccess: afterSubmit
    })
)(_Filters);