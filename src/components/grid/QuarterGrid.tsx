import React from 'react';
import styled from 'styled-components';

const SWrapper = styled.div`
    display: flex;
    width: 100%;
`;

const SColumn = styled.div`
    width: 25%;
    padding: 0 5px;
    box-sizing: border-box;

    &:first-child {
        padding-left: 0;
    }

    &:last-child {
        padding-right: 0;
    }
`;

interface IQuarterGridProps {
    children: React.ReactNodeArray | React.ReactChild;
}

export const QuarterGrid: React.StatelessComponent<IQuarterGridProps> = (props: IQuarterGridProps) => {
    return(
        <SWrapper>
            <SColumn>{props.children[0]}</SColumn>
            <SColumn>{props.children[1]}</SColumn>
            <SColumn>{props.children[2]}</SColumn>
            <SColumn>{props.children[3]}</SColumn>
        </SWrapper>
    );
}