import React from 'react';
import styled from 'styled-components';
import { COLORS } from 'constants/colors';

const SWrapper = styled.div`
    padding: 20px 0;
    margin-top: 30px;
    border-bottom: 2px solid ${COLORS.GRAY};
    box-sizing: border-box;
    display: flex;
    align-items: center;
`;

const SText = styled.h2`
    color: ${COLORS.RED};
    font-size: 24px;
    font-weight: 400;
    margin: 0;
    text-transform: capitalize;
`;

interface IPageTitleProps {
    text: string;
}

export const PageTitle: React.StatelessComponent<IPageTitleProps> = (props: IPageTitleProps) => {
    return(
        <SWrapper>
            <SText>{props.text}</SText>
        </SWrapper>
    );
}