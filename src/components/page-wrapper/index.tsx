import React from 'react';
import styled from 'styled-components';
import { NavBar } from 'modules/navbar';

const SSection = styled.section`
    width: 80%;
    margin: 0 auto;
    padding: 15px;
`;

interface IPageWrapper {
    children: React.ReactChild;
}

export const PageWrapper: React.StatelessComponent<IPageWrapper> = (props: IPageWrapper) => {
    return(
        <React.Fragment>
            <NavBar />
            <SSection>
                {props.children}
            </SSection>
        </React.Fragment>
    );
};