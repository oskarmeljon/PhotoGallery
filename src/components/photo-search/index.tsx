import React, { Component } from 'react';
import { InjectedFormProps, reduxForm, reset } from 'redux-form';
import styled from 'styled-components';
import { COLORS } from 'constants/colors';
import { Dispatch } from 'redux';
import { FormInput } from 'components/redux-form/input/field';

const SWrapper = styled.div`
    width: 100%;
    margin: 15px auto;
    border-bottom: 2px solid ${COLORS.GRAY};
`;

const SSearchWrapper = styled.div`
    display: flex;
    width: 100%;
`;

const SButton = styled.button`
    border: none;
    cursor: pointer;
    font-size: 16px;
    box-shadow: 1px 1px 3px -1px ${COLORS.GRAY};
    box-shadow: 1px 1px 3px 0 rgba(0, 0, 0, .07);
    background: ${COLORS.RED};
    color: ${COLORS.WHITE};
    padding: 16px 0;
    margin-left: -2px;
    width: 160px;
    transition: .5s ease-in-out;
`;

const SText = styled.p`
    color: ${COLORS.DARK_BLUE};
    font-size: 14px;
    margin: 30px 0;
`;

interface ISearchPhotoProps {
    tags: string;
    searchFunction: (fields: any) => Promise<void>;
    setTags: (fields: any) => Promise<void>;
}

class _SearchPhoto extends Component<InjectedFormProps<{}, ISearchPhotoProps> & ISearchPhotoProps> {
    public render() {
        return(
            <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                <SWrapper>
                    <SSearchWrapper>
                        <FormInput 
                            name="Tags"
                            placeholder="Search by tags..."
                        />
                        <SButton type="submit">Search</SButton>
                    </SSearchWrapper>
                    <SText>By default photos are taken by tag <strong>dogs</strong>, right now you are searching by <strong>{this.props.tags}</strong></SText>
                </SWrapper>
            </form>
        );
    }

    private onSubmit = async (fields: any) => {
        if (fields.Tags !== undefined) {
            const createString: string = fields.Tags.split(' ').join();
            this.props.setTags(createString);
            this.props.searchFunction(createString);
        }
    }
}

const FORM_NAME = 'SEARCH_PHOTOS';

const afterSubmit = (_: any, dispatch: Dispatch) => dispatch(reset(FORM_NAME));

export const SearchPhoto = reduxForm<{}, ISearchPhotoProps>({
    form: FORM_NAME,
    onSubmitSuccess: afterSubmit
})(_SearchPhoto);