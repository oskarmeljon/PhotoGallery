import React from 'react';
import styled from 'styled-components';
import { createPhotoSourceLink, formatString } from 'utilities/util';
import { COLORS } from 'constants/colors';
import { Link } from 'react-router-dom';

const SWrapper = styled.div`
    position: relative;
    width: 20%;
    height: 200px;
    overflow: hidden;
    padding: 5px;
    box-sizing: border-box;
`;

const SImg = styled.img`
    display: block;
    width: 100%;
    height: auto;
`;

const SOverlay = styled.div`
    position: absolute;
    top: 5px;
    left: 5px;
    height: 100%;
    width: calc(100% - 10px);
    opacity: 0;
    transition: .3s ease-in-out;
    background: rgba(0, 0, 0, 0.5);

    &:hover {
        opacity: 1;
    }
`;

const SOverlayWrapper = styled.div`
    position: absolute;
    bottom: 5px;
    left: 0;
    width: 100%;
    padding: 5px;
    box-sizing: border-box;
`;

const STextWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
`;

const STitle = styled.a`
    color: ${COLORS.WHITE};
    font-weight: 600;
    font-size: 14px;
    letter-spacing: .5px;
    margin-top: 0;
    margin-bottom: 5px;
    text-decoration: none;

    &:hover {
        color: ${COLORS.RED};
    }
`;

const SCreator = styled(Link)`
    color: lightgray;
    font-weight: 500;
    font-size: 11px;
    text-decoration: none;

    &:hover {
        color: ${COLORS.RED};
    }
`;

const SText = styled.p`
    margin: 2px 0;
    font-size: 11px;
    color: ${COLORS.WHITE};
`;

const SDescriptionWrapper = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    padding: 5px;
    box-sizing: border-box;
`;

const SDescription = styled.p`
    margin: 2px 0;
    font-size: 11px;
    color: ${COLORS.WHITE};
`;

interface IPhotoProps {
    farmId: number;
    serverId: number;
    id: number;
    secret: number;
    title: string;
    owner: string;
    creator: string;
    description: string;
    date: Date | string;
}

export const Photo: React.StatelessComponent<IPhotoProps> = (props: IPhotoProps) => {
    const formatDate = (date: string | Date) => {
        const newDate = new Date(date);
        return `${newDate.getDate()}/${newDate.getMonth()}/${newDate.getFullYear()}`;
    };

    return(
        <SWrapper>
            <SImg src={createPhotoSourceLink(props.farmId, props.serverId, props.id, props.secret)} alt=""/>
            <SOverlay>
                <SDescriptionWrapper>
                    <SDescription>
                        {props.description}
                    </SDescription>
                </SDescriptionWrapper>
                <SOverlayWrapper>
                    <STextWrapper>
                        <STitle 
                            href={props.title === '' || props.title === 'No text' ? '#' : createPhotoSourceLink(props.farmId, props.serverId, props.id, props.secret)}
                            target="_blank"
                        >
                            {formatString(props.title)}
                        </STitle>
                        <SText>
                            by <SCreator to={`/photos/${props.owner}`}>{formatString(props.creator)}</SCreator>, {formatDate(props.date)}
                        </SText>
                    </STextWrapper>
                </SOverlayWrapper>
            </SOverlay>
        </SWrapper>
    );
}