import React from 'react';
import { Field, BaseFieldProps } from 'redux-form';
import { Input, IInputProps } from 'components/redux-form/input/input';

class ReduxField extends Field<any> {}

interface IFormInputProps extends IInputProps, BaseFieldProps {
    type?: string;
    placeholder?: string;
}

export const FormInput: React.SFC<IFormInputProps> = (props: IFormInputProps) => (
        <ReduxField
            component={Input}
            {...props}
        />
);
