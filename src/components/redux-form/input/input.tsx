import React from 'react';
import styled from 'styled-components';
import { COLORS } from 'constants/colors';

const SInput = styled.input`
    color: #1a1a1a;
    border: 2px solid #e6e6e6;
    background-color: #fff;
    padding: 16px 24px;
    flex: 1;
    min-height: 50px;
    width: 100%;
    transition: .5s ease-in-out;
    box-sizing: border-box;

    &:focus {
        outline: none;
        border: 2px solid ${COLORS.RED};
    }
`;

export interface IInputProps {
    disabled?: boolean;
    input?: any;
    type?: string;
    placeholder?: string;
}

export const Input: React.StatelessComponent<IInputProps> = (props: IInputProps) => (
    <SInput
        type={props.type}
        disabled={props.disabled} 
        placeholder={props.placeholder}
        {...props.input} 
    />
);