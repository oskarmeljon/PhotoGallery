import React from 'react';
import { Field, BaseFieldProps } from 'redux-form';
import { Select } from 'components/redux-form/select';
import { IOption } from './select';

export interface ISelectProps {
    value?: any;
    options: IOption[];
}

class ReduxField extends Field<any> {}

interface IFormSelectProps extends ISelectProps, BaseFieldProps {}

export const FormSelect: React.StatelessComponent<IFormSelectProps> = (props: IFormSelectProps) => (
        <ReduxField
            component={Select}
            {...props}
        />
);
