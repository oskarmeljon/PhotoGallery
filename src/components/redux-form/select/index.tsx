import React, { Component, ChangeEvent } from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { change, FormAction } from 'redux-form';
import { Select as SelectComponent, IOption } from 'components/redux-form/select/select';

interface IMapDispatchToProps {
    changeSelectValue: (form: string, field: string, value: any) => FormAction;
}

export interface ISelectProps extends IMapDispatchToProps {
    value?: any;
    options: IOption[];
    meta?: any;
    input?: any;
    onChange: (e: ChangeEvent) => void;
}

class _Select extends Component<ISelectProps> {

    public componentDidMount() {
        this.setReduxFormValue(this.props.options[0].text);
    }

    public render() {
        return(
            <SelectComponent
                value={this.props.value}
                options={this.props.options}
                onChange={(e: any) => this.onChange(e)}
            />
        );
    }

    private setReduxFormValue = (value: any) => {
        if(this.props.meta) {
            const form = this.props.meta.form;
            const field = this.props.input.name;
            this.props.changeSelectValue(form, field, value);
        }
    }

    private onChange = (event: any) => {
        this.setReduxFormValue(event.target.value);

        if(this.props.onChange) {
            this.props.onChange(event.target.value);
        }
    };
}

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, Action>): IMapDispatchToProps => ({
    changeSelectValue: (form: string, field: string, value: any) => dispatch(change(form, field, value))
});

export const Select = connect(undefined, mapDispatchToProps)(_Select);