import React, { Component, ChangeEvent } from 'react';
import styled from 'styled-components';
import { COLORS } from 'constants/colors';

const SSelect = styled.select`
    color: #1a1a1a;
    border: 2px solid #e6e6e6;
    background-color: #fff;
    padding: 12px 24px;
    flex: 1;
    min-height: 50px;
    width: 100%;
    transition: .5s ease-in-out;
    box-sizing: border-box;

    &:focus {
        outline: none;
        border: 2px solid ${COLORS.RED};
    }
`;

export interface IOption {
    value: string | number;
    text: string;
}

export interface ISelectProps {
    value: any;
    options: IOption[];
    onChange: (e: ChangeEvent) => void;
}

export class Select extends Component<ISelectProps> {
    public render() {
        const mapedOptions = this.createOptionsForSelect();

        return(
            <SSelect
                value={this.props.value}
                onChange={this.props.onChange}
            >
                {mapedOptions}
            </SSelect>
        );
    }

    private createOptionsForSelect = (): any => {
        return this.props.options.map((option: IOption, index: number) => {
            return(
                <option 
                    key={index}
                    value={option.value}
                >
                    {option.text}
                </option>
            );
        });
    }
}