import React from 'react';
import styled from 'styled-components';

const SWrapper = styled.div`
    width: 1440px;
    margin: 0 auto;
`;

interface IWrapper {
    children: React.ReactChild;
}

export const Wrapper: React.StatelessComponent<IWrapper> = (props: IWrapper) => (
    <SWrapper>{props.children}</SWrapper>
);