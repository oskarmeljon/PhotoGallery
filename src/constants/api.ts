const API_DETAILS = {
    PREFIX: 'https://api.flickr.com/services/rest/?method=',
    METHODS: {
        GET_PHOTOS: 'flickr.photos.search',
        GET_AUTHOR_PHOTOS: 'flickr.people.getPublicPhotos'
    },
    ACCESS_KEY: 'db39027afb09a8be87085234c96843cf',
    FORMAT: 'format=json&nojsoncallback=1',
    EXTRAS: 'extras=description,date_upload,date_taken,views,realname'
};

export enum Sort {
    'date-posted-asc' = 'Date posted ascending',
    'date-posted-desc' = 'Date posted descending', 
    'date-taken-asc' = 'Date taken ascending', 
    'date-taken-desc' = 'Date taken descending', 
    'interestingness-desc' = 'Interestingness descending', 
    'interestingness-asc' = 'Interestingness ascending',
    'relevance' = 'Relevance'
}

export enum License {
    'All Rights Reserved',
    'Attribution-NonCommercial-ShareAlike License',
    'Attribution-NonCommercial License',
    'Attribution-NonCommercial-NoDerivs License',
    'Attribution License',
    'Attribution-ShareAlike License',
    'Attribution-NoDerivs License',
    'No known copyright restrictions',
    'United States Government Work',
    'Public Domain Dedication (CC0)',
    'Public Domain Mark'
}

export interface IGetPhotosFilters {
    sort: Sort | string;
    license: License | string;
    minTakenDate: Date | string;
    maxTakenDate: Date | string;
}

export class API {
    public static GetPhotos = (tags: string, page: number, per_page: number, filters: IGetPhotosFilters = {
        sort: '',
        license: '',
        minTakenDate: '',
        maxTakenDate: ''
    }) => {
        const basicUrl = `${API_DETAILS.PREFIX}${API_DETAILS.METHODS.GET_PHOTOS}&api_key=${API_DETAILS.ACCESS_KEY}&text=${tags}&${API_DETAILS.FORMAT}&${API_DETAILS.EXTRAS}&page=${page}&per_page=${per_page}`
        
        return `${basicUrl}&sort=${filters.sort}&license=${filters.license}&min_taken_date=${filters.minTakenDate}&max_taken_date=${filters.maxTakenDate}`;
    };
    
    public static GetAuthorPhotos = (authorId: string) => `${API_DETAILS.PREFIX}${API_DETAILS.METHODS.GET_AUTHOR_PHOTOS}&api_key=${API_DETAILS.ACCESS_KEY}&user_id=${authorId}&${API_DETAILS.FORMAT}&${API_DETAILS.EXTRAS}`;
}