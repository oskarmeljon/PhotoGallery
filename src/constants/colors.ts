export const COLORS = {
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    DARK_BLUE: '#13191E',
    RED: '#E91E63',
    GRAY: '#e6e6e6',
};