import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import store from './store';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { RoutingConfiguration } from 'views/index';
import { PageWrapper } from 'components/page-wrapper';
import 'styles/normalize.css';
import 'styles/main.css';

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <PageWrapper>
                <RoutingConfiguration />
            </PageWrapper>
        </BrowserRouter>
    </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();

