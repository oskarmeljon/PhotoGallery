import React, { Component } from 'react';
import styled from 'styled-components';
import { IPhoto } from 'reducers/photos/models';
import { Photo } from 'components/photo';
import { Loader } from 'components/loader';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { connect } from 'react-redux';
import { IStore } from 'reducers/index';
import { getAuthorPhotos } from 'actions/photos';

const SSection = styled.div`
    width: 100%;
    margin: 0 auto;
    padding: 15px 0;
    display: flex;
    flex-wrap: wrap;
`;

interface IMapStateToProps {
    photos: IPhoto[];
    isLoading: boolean;
}

interface IMapDispatchToProps {
    getAuthorPhotos: (authorId: string) => Promise<void>;
}

interface IPhotosListProps extends IMapStateToProps, IMapDispatchToProps {
    authorId: string;
}

class _PhotosList extends Component<IPhotosListProps> {

    public componentDidMount() {
        this.props.getAuthorPhotos(this.props.authorId);
    }

    public render() {
        const photos = this.props.photos.map((photo: IPhoto, index: number) => {
            return( 
                <Photo 
                    key={index} 
                    farmId={photo.farm} 
                    serverId={photo.server} 
                    id={photo.id} 
                    secret={photo.secret} 
                    title={photo.title}
                    owner={photo.owner}
                    creator={photo.realname}
                    date={photo.datetaken}
                    description={photo.description._content}
                />
            );
        });

        if (this.props.isLoading) {
            return <Loader />;
        }

        return(
            <React.Fragment>
                <SSection>
                    {photos}
                </SSection>
            </React.Fragment>
        );
    };
}

const mapStateToProps = (state: IStore): IMapStateToProps => ({
    photos: state.photos.authorPhotos.photo,
    isLoading: state.photos.isLoading
});

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, Action>): IMapDispatchToProps => ({
    getAuthorPhotos: (authorId: string) => dispatch(getAuthorPhotos(authorId)),
});

export const PhotosList = connect(mapStateToProps, mapDispatchToProps)(_PhotosList);