import React, { Component } from 'react';
import styled from 'styled-components';
import { IPhoto } from 'reducers/photos/models';
import { Photo } from 'components/photo';
import { Loader } from 'components/loader';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { connect } from 'react-redux';
import { IStore } from 'reducers/index';
import { getPhotos } from 'actions/photos';
import { IGetPhotosFilters } from 'constants/api';

const SSection = styled.div`
    width: 100%;
    margin: 0 auto;
    padding: 15px 0;
    display: flex;
    flex-wrap: wrap;
`;

interface IMapStateToProps {
    photos: IPhoto[];
    isLoading: boolean;
    page: number;
    perPage: number;
    totalPages: number;
    tags: string | string[];
}

interface IMapDispatchToProps {
    getPhotos: (tags?: string,page?: number, per_page?: number, filters?: IGetPhotosFilters, onSuccess?: any) => Promise<void>;
}

interface ILandingPageProps extends IMapStateToProps, IMapDispatchToProps {}

interface ILandingPageState {
    page: number;
    perPage: number;
    tags: string;
}

class _PhotosList extends Component<ILandingPageProps, ILandingPageState> {
    public state: ILandingPageState = {
        page: 1,
        perPage: 100,
        tags: 'dogs'
    }

    public componentDidMount() {
        this.props.getPhotos(this.state.tags, this.props.page, this.props.perPage);
    }

    public render() {
        const photos = this.props.photos.map((photo: IPhoto, index: number) => {
            return( 
                <Photo 
                    key={index} 
                    farmId={photo.farm} 
                    serverId={photo.server} 
                    id={photo.id} 
                    secret={photo.secret} 
                    title={photo.title}
                    owner={photo.owner}
                    creator={photo.realname}
                    date={photo.datetaken}
                    description={photo.description._content}
                />
            );
        });

        if (this.props.isLoading) {
            return <Loader />;
        }

        return(
            <React.Fragment>
                <SSection>
                    {photos}
                </SSection>
            </React.Fragment>
        );
    };
}

const mapStateToProps = (state: IStore): IMapStateToProps => ({
    photos: state.photos.photo,
    isLoading: state.photos.isLoading,
    page: state.photos.page,
    perPage: state.photos.perpage,
    totalPages: state.photos.pages,
    tags: state.photos.tags
});

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, Action>): IMapDispatchToProps => ({
    getPhotos: (tags?: string, page?: number, perPage?: number, onSuccess?: any) => dispatch(getPhotos(tags, page, perPage, onSuccess)),
});

export const PhotosList = connect(mapStateToProps, mapDispatchToProps)(_PhotosList);