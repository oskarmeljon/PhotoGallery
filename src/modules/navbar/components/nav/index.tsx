import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { COLORS } from 'constants/colors';

const SNav = styled.nav`
    display: flex;
    align-items: center;
    list-style: none;
    margin: 0 15px;
`;

const SNavItem = styled.li`
    margin: 0 10px;

    &:first-item {
        margin-left: 0;
    }
`;

const SLink = styled(Link)`
    display: block;
    font-weight: 400;
    font-size: 18px;
    color: #fff;
    letter-spacing: 0.5px;
    line-height: 1;
    transition: color .4s ease-in-out;
    text-decoration: none;
    padding: 8px;
    position: relative;

    &:hover {
        color: ${COLORS.RED};
    }
`;

export const Navigation: React.StatelessComponent = () => {
    return(
        <SNav>
            <SNavItem><SLink to="/photos">Homepage</SLink></SNavItem>
            <SNavItem><SLink to="/dogs-map">Dogs map</SLink></SNavItem>
        </SNav>
    );
};