import React from 'react';
import styled from 'styled-components';
import { COLORS } from 'constants/colors';
import { Navigation } from 'modules/navbar/components/nav';

const SHeader = styled.header`
    background: ${COLORS.DARK_BLUE};
    color: ${COLORS.WHITE};
    height: 72px;
    display: flex;
    align-items: center;
    padding: 0 20px;
`;

const STitle = styled.h1`
    color: ${COLORS.RED};
    margin: 0;
    font-weight: 500;
    font-size: 25px;
`;

export const NavBar: React.StatelessComponent = () => {
    return(
        <SHeader>
            <STitle>PhotoGallery</STitle>
            <Navigation />
        </SHeader>
    );
};