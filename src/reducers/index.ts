import { combineReducers } from 'redux';
import { reducer as photos, IPhotosStoreState } from 'reducers/photos';
import { reducer as form } from 'redux-form';

export interface IStore {
    readonly photos: IPhotosStoreState;
    readonly form: any;
}

export default combineReducers({
    photos,
    form
});