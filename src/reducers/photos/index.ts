import { IPhoto, IAuthorPhotos } from './models';
import { TYPES } from 'action-types/photos';
import { IAction } from 'reducers/photos/models';
 
export interface IPhotosStoreState {
    page: number;
    pages: number;
    perpage: number;
    total: number;
    photo: IPhoto[],
    isLoading: boolean;
    isError: boolean;
    tags: string;
    error: any;
    authorPhotos: IAuthorPhotos;
}

const defaultState: IPhotosStoreState = {
    page: 1,
    pages: 1,
    perpage: 100,
    total: 0,
    photo: [
        {
            farm: 0,
            id: 0,
            isfamily: 0,
            isfriend: 0,
            ispublic: 0,
            owner: '',
            secret: 0,
            server: 0,
            title: '',
            realname: '',
            description: {
                _content: ''
            },
            datetaken: '',
            dateupload: ''
        }
    ],
    isLoading: false,
    isError: false,
    tags: 'dogs',
    error: null,
    authorPhotos: {
        page: 1,
        pages: 1,
        perpage: 100,
        total: 0,
        photo: [
            {
                datetaken: '',
                datetakengranularity: 0,
                datetakenunknown: 0,
                dateupload: '',
                description: {
                    _content: ''
                },
                farm: 0,
                id: 0,
                isfamily: 0,
                isfriend: 0,
                ispublic: 0,
                owner: '',
                realname: '',
                secret: 0,
                server: 0,
                title: '',
                views: 0
            }
        ]
    }
};

export const reducer = (state: IPhotosStoreState = defaultState, action: IAction): IPhotosStoreState => {
    switch (action.type) {
        case TYPES.LOAD_PHOTO.PENDING:
            return {
                ...state,
                isLoading: action.isLoading,
                isError: action.isError
            };
        case TYPES.LOAD_PHOTO.FULFILLED:
            return {
                ...state,
                ...action.data,
                isLoading: action.isLoading,
                isError: action.isError
            };
        case TYPES.LOAD_PHOTO.REJECTED:
            return {
                ...state,
                isError: action.isError,
                error: action.error
            };
        case TYPES.SET_TAGS.PENDING:
            return {
                ...state,
                isLoading: action.isLoading,
                isError: action.isError
            };
        case TYPES.SET_TAGS.FULFILLED:
            return {
                ...state,
                isLoading: action.isLoading,
                isError: action.isError,
                tags: action.tags
            };
        case TYPES.SET_TAGS.REJECTED:
            return {
                ...state,
                isLoading: action.isLoading,
                isError: action.isError,
                error: action.error
            };
        case TYPES.LOAD_AUTHOR_PHOTOS.PENDING:
            return {
                ...state,
                isLoading: action.isLoading,
                isError: action.isError
            };
        case TYPES.LOAD_AUTHOR_PHOTOS.FULFILLED:
            return {
                ...state,
                isLoading: action.isLoading,
                isError: action.isError,
                authorPhotos: action.authorPhotos
            };
        case TYPES.LOAD_AUTHOR_PHOTOS.REJECTED:
            return {
                ...state,
                isLoading: action.isLoading,
                isError: action.isError,
                error: action.error
            };
        default:
            return state;
    }
};
