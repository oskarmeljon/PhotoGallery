export interface IAction {
    readonly type: string;
    readonly data?: any;
    readonly error?: any;
    readonly isLoading: boolean;
    readonly isError: boolean;
    readonly tags: string;
    readonly authorPhotos: any;
}

export interface IPhoto {
    farm: number;
    id: number;
    isfamily: boolean | number;
    isfriend: boolean | number;
    ispublic: boolean | number;
    owner: string;
    secret: number;
    server: number;
    title: string;
    realname: string;
    description: {
        _content: string;
    };
    datetaken: Date | string;
    dateupload: Date | string;
}

export interface IAuthorPhoto {
    datetaken: Date | string;
    datetakengranularity: number;
    datetakenunknown: number;
    dateupload: Date | string;
    description: {
        _content: string;
    }
    farm: number;
    id: number;
    isfamily: boolean | number;
    isfriend: boolean | number;
    ispublic: boolean | number;
    owner: string;
    realname: string;
    secret: number;
    server: number;
    title: string;
    views: number;
}

export interface IPhotos {
    page: number;
    pages: number;
    perpage: number;
    total: number;
    photo: IPhoto[],
    isLoading: boolean;
    isError: boolean;
    tags: string;
    error: any;
    authorPhotos: IAuthorPhotos | null;
};

export interface IAuthorPhotos {
    page: number;
    pages: number;
    perpage: number;
    total: number;
    photo: IAuthorPhoto[];
}

export interface IData {
    photos: IPhotos;
    stat: string;
}