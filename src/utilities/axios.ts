import axios from 'axios';

export const callApiGet = (url: string) => {
    return axios.get(url);
};

export const callApiPost = (url: string, body: any) => {
    return axios.post(url, body);
};