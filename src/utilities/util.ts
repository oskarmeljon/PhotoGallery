interface IExtendAction {
    PENDING: string;
    FULFILLED: string;
    REJECTED: string;
}

export const extendAction = (actionType: string): IExtendAction => ({
    PENDING: `${actionType}_PENDING`,
    FULFILLED: `${actionType}_FULFILLED`,
    REJECTED: `${actionType}_REJECTED`
})

export const createPhotoSourceLink = (farmId: number, serverId: number, id: number, secret: number) => {
    return `https://farm${farmId}.staticflickr.com/${serverId}/${id}_${secret}.jpg`;
}

export const formatString = (data: string | null): string => {
    if ((data === null) || (data === undefined) || (data.length === 0)) {
        return 'No text';
    }
    return data;
}

export const convertEnumToOptionsArray = (data: any) => {
    return Object.keys(data).map(key => ({ 
        text: data[key],
        value: key 
    }));
}

export const convertEnumToOptionsArrayByValue = (data: any) => {
    const keys = Object.keys(data).filter(k => typeof data[k as any] === 'number');
    return keys.map((key: string, index: number) => {
        return {
            text: key,
            value: index
        };
    });
}

export const mapToOptions = (data: any[]) => data.map((item: string) => {
    return {
        value: item,
        text: item
    };
});