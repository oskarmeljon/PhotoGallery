import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { IStore } from 'reducers/index';
import { PhotosList } from 'modules/gallery/author/containers/PhotosList';
import { PageTitle } from 'components/page-title';

interface IRouterParams {
    authorId: string;
}

interface IMapStateToProps {
    isLoading: boolean;
    realName: string;
}

interface IAuthorPageProps extends RouteComponentProps<IRouterParams>, IMapStateToProps {}

class _AuthorPage extends Component<IAuthorPageProps> {
    public render() {
        return(
            <React.Fragment>
                <PageTitle text={this.props.realName} />
                <PhotosList authorId={this.props.match.params.authorId}/>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: IStore): IMapStateToProps => ({
    isLoading: state.photos.isLoading,
    realName: state.photos.authorPhotos.photo[0].realname,
});

export const AuthorPage = connect(mapStateToProps)(_AuthorPage);