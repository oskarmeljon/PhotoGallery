import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { LandingPage } from 'views/gallery/main';
import { AuthorPage } from 'views/gallery/author';

export const GalleryRoutingConfiguration = () => {
    return(
        <Switch>
            <Route exact={true} path="/photos/" component={LandingPage} />
            <Route path="/photos/:authorId" component={AuthorPage} />
        </Switch>
    )
}