import React, { Component } from 'react';
import { PhotosList } from 'modules/gallery/main/containers/PhotosList';
import { SearchPhoto } from 'components/photo-search';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { getPhotos, setTags } from 'actions/photos';
import { IStore } from 'reducers/index';
import { ErrorDialog } from 'components/dialogs/error';
import { Filters } from 'components/filters';
import { IGetPhotosFilters } from 'constants/api';

interface IMapStateToProps {
    tags: string;
    isError: boolean;
}

interface IMapDispatchToProps {
    getPhotos: (tags: string, page?: number, perPage?: number, filters?: IGetPhotosFilters, onSuccess?: () => void) => Promise<void>;
    setTags: (tags: string) => Promise<void>;
}

interface ILandingPageProps extends IMapStateToProps, IMapDispatchToProps {}

class _LandingPage extends Component<ILandingPageProps> {
    public isVisible: boolean = this.props.isError;

    public render() {
        return(
            <React.Fragment>
                <SearchPhoto 
                    searchFunction={this.props.getPhotos}
                    setTags={this.props.setTags}
                    tags={this.props.tags}
                />
                { this.props.isError && <ErrorDialog 
                    visible={this.isVisible} 
                    onClose={this.closeDialog}
                /> }
                <Filters />
                <PhotosList />
            </React.Fragment>
        );
    }

    public closeDialog = () => {
        if (this.isVisible === true) {
            this.isVisible = false;
        }
    }
}

const mapStateToProps = (state: IStore): IMapStateToProps => ({
    tags: state.photos.tags,
    isError: state.photos.isError
});

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, Action>): IMapDispatchToProps => ({
    getPhotos: (tags: string = 'dogs', page: number = 1, perPage: number = 100, filters?: IGetPhotosFilters, onSuccess?: () => void) => dispatch(getPhotos(tags, page, perPage, filters, onSuccess)),
    setTags: (tags: string) => dispatch(setTags(tags))
});

export const LandingPage = connect(mapStateToProps, mapDispatchToProps)(_LandingPage);