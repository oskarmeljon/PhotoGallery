import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { GalleryRoutingConfiguration } from 'views/gallery';

export const RoutingConfiguration = () => {
    return(
        <Switch>
            <Route path="/photos" component={GalleryRoutingConfiguration} />
            <Redirect to="/photos" />
        </Switch>
    )
}